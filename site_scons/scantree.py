import fnmatch
import glob
import os

from SCons.Script import *

DEFAULT_SRC_PATTERNS = ["*.c", "*.cpp"]
DEFAULT_INCLUDE_PATTERNS = ["*.h", ".hpp"]

def scan_tree(dirnode, src_patterns=None, include_patterns=None):
    if src_patterns is None:
        src_patterns = DEFAULT_SRC_PATTERNS
    if include_patterns is None:
        include_patterns = DEFAULT_INCLUDE_PATTERNS
        
    src_filenodes = []
    src_dirnodes = []
    include_dirnodes = []
    
    for dirpath, dirnames, filenames in os.walk(os.path.relpath(dirnode.abspath)):
        for src_pattern in src_patterns:
            matching_src_files = Glob(os.path.join(dirpath, src_pattern))
            src_filenodes.extend(matching_src_files)
            if Dir(dirpath) not in src_dirnodes:
                src_dirnodes.append(Dir(dirpath))
           
        for include_pattern in include_patterns:
            if (len(fnmatch.filter(filenames, include_pattern)) > 0) and (Dir(dirpath) not in include_dirnodes):
                include_dirnodes.append(Dir(dirpath))
    return (src_filenodes, src_dirnodes, include_dirnodes)