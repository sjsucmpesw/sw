"""
Console handler
"""

__author__ = "jtran"
__version__ = "0.1"


# Command Identifiers
UNDEFINED = "UNDEFINED"
HELP = "help"
ENABLE_FEED = "enable feed"
DISABLE_FEED = "disable feed"
QUIT = "quit"

_cmd_dict = {
    "help": HELP,
    "enable feed": ENABLE_FEED,
    "disable feed": DISABLE_FEED,
    "quit": QUIT
}


def handle_console(prompt="Command: ", callback=None, callback_args=None, cmd_callback=None):
    """
    Simple console handler routine
      - Prompt user for a console input
    :param prompt: Prompt string (str)
    :param callback: Callback function (func ptr)
    :param callback_args: Callback function arguments (iterable obj)
    :param cmd_callback: Command to invoke callback function (str)
    :return: tuple(command (str), actual input (str))
    """

    con_input = raw_input(prompt)
    if con_input in _cmd_dict:
        ret = _cmd_dict[con_input]
    else:
        ret = UNDEFINED

    if callback is not None and cmd_callback is not None:
        if con_input == cmd_callback:
            if callback_args is None:
                callback()
            else:
                callback(*callback_args)

    return ret, con_input
