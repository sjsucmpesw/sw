"""
Transactor client
"""

import logging

from ftdev import FtDev

# Transactor commands
CMD_TEMP = 0x1


class TransactorClient(object):
    def __init__(self, dev_id=0, baud=9600, rd_timeout_ms=1000, **kwargs):
        self._logger = kwargs.get("logger", logging.getLogger(__name__))
        self._ftd_handle = FtDev(logger=self._logger)
        self._ftd_handle.open(dev_id=dev_id, baud=baud, rd_timeout_ms=rd_timeout_ms)

        # Data from the Transactor
        self._temperature = 0  # Temperature from the temperature sensor / [F]

    """
    Public methods
    """
    def update_temp(self):
        error = 0
        num_writes = self._ftd_handle.write([CMD_TEMP])
        messages = self._ftd_handle.read(count=1)
        if len(messages) <= 0:
            error = 1
        else:
            self._temperature = messages[0]
        return self._temperature, error

    """
    Accessors
    """
    @property
    def temperature(self):
        return self._temperature




if __name__ == "__main__":  # Test
    transactor_client = TransactorClient()
    import time
    while True:
        _temp, _error = transactor_client.update_temp()
        print _temp, _error
        time.sleep(1)
