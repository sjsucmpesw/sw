"""
FTDI FTD2xx Wrapper
  - Intended for FT23X
"""

import logging

from ftd2xx import ftd2xx, defines

__author__ = "jtran"
__version__ = "0.1"


class FtDev(object):
    def __init__(self, **kwargs):
        self._ftd_handle = None
        self._logger = kwargs.get("logger", logging.getLogger(__name__))

    """
    Public methods
    """
    def open(self, dev_id=0, baud=9600, rd_timeout_ms=1000, wr_timeout_ms=1000):
        self._ftd_handle = ftd2xx.open(dev_id)
        self._ftd_handle.setBaudRate(baud)
        self._ftd_handle.setTimeouts(rd_timeout_ms, wr_timeout_ms)
        self._ftd_handle.setDataCharacteristics(wordlen=defines.BITS_8, stopbits=defines.STOP_BITS_1, parity=defines.PARITY_NONE)

    def write(self, messages):
        assert isinstance(self._ftd_handle, ftd2xx.FTD2XX)
        error = self._ftd_handle.write(data="".join(self._normalize_messages_datatype(messages)))
        return error

    def read(self, count=1):
        assert isinstance(self._ftd_handle, ftd2xx.FTD2XX)
        messages = self._ftd_handle.read(nchars=count)
        return map(lambda char: ord(char), list(messages))

    def set_timeout(self, rd_timeout_ms=1000, wr_timeout_ms=1000):
        assert isinstance(self._ftd_handle, ftd2xx.FTD2XX)
        self._ftd_handle.setTimeouts(rd_timeout_ms, wr_timeout_ms)

    def reset(self):
        assert isinstance(self._ftd_handle, ftd2xx.FTD2XX)
        self._ftd_handle.resetDevice()

    """
    Private methods
    """
    @staticmethod
    def _normalize_messages_datatype(messages):
        normalized_messages = []
        for message in messages:
            if isinstance(message, int):
                normalized_messages.append(chr(message))
            elif isinstance(message, str):
                normalized_messages.append(message)
            else:
                normalized_messages.append(message)
        return normalized_messages

    def get_read_buff(self):
        assert isinstance(self._ftd_handle, ftd2xx.FTD2XX)




if __name__ == "__main__":  # Test
    ft_dev = FtDev()
    ft_dev.open(0, 9600, rd_timeout_ms=1000)
    _num_writes = ft_dev.write([0x48, "i"])
    _messages = ft_dev.read(2)
    print "Wrote: %s" % _num_writes
    print _messages
