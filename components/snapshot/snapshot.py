#!/usr/bin/python
"""
Snapshot
"""

import datetime
import logging
import os
import time

import cv2

_CLASSIFIER_FILEPATH = r"haarcascade_frontalface_default.xml"
_SNAPSHOT_VFEED = "Snapshot Video Feed"
feed_flag = False


class Snapshot:
    def __init__(self, camera_port=0, classifier_filepath=_CLASSIFIER_FILEPATH, **kwargs):
        self._video_capture_obj = cv2.VideoCapture(int(camera_port))
        self._object_classifier_obj = cv2.CascadeClassifier(str(classifier_filepath))
        self._enable_vfeed = True

        self._logger = kwargs.get("logger", logging.getLogger(__name__))

    def __del__(self):
        self._video_capture_obj.release()
        cv2.destroyAllWindows()

    """
    Public methods
    """
    def save_image(self, image_obj, image_filepath=None, metadata=None):
        """
        Save an image to file
        :param image_filepath: Output image file path (str)
        :param image_obj: Image object (Image)
        :param metadata: A list of metadata (list of str)
        :raise: IOError
        """
        if image_filepath is None:
            image_filename = "{filename}.{ext}".format(filename=self.current_time(), ext="jpg")
            image_filepath = os.path.join(os.path.dirname(__file__), image_filename)

        if metadata is not None:
            for index, metadata_str in enumerate(metadata):
                cv2.putText(img=image_obj, text=str(metadata_str), org=(1, 15 + (15 * index)),
                            fontFace=cv2.FONT_HERSHEY_PLAIN, fontScale=1.0, color=(0, 0, 255),
                            thickness=1)
        cv2.imwrite(image_filepath, image_obj)
        self._logger.debug("Saved image: [%s]", image_filepath)

    def search_for_object(self, duration=2, timeout=5, mark_objects=True, display_feed=False):
        """
        Search for an object
        :param duration: Minimum duration; there must be at least (1) detected target for the given duration (float)
        :param timeout: Timeout; maximum time to search before returning (float)
        :param mark_objects: Mark detected objects (bool)
        :param display_feed: Display a visual feed during detection (bool)
        :return:
            - If target detected for duration, return image obj
            - Else, timeout occurred, return None
        :raise: IOError
        """
        timeout = max(timeout, 0)
        if (duration > timeout) and (timeout != 0):
            self._logger.warning("The given duration is greater than the given timeout")
            return None

        rel_elapsed_time = 0
        rel_detected_duration = 0
        ret_image = None
        while (rel_elapsed_time < timeout) or (timeout == 0):
            image_obj, error = self.read_image()
            if error:
                self._logger.warning("Unable to read image from the capture device.")
                break

            detection_start_time = time.time()
            image_obj, detected = self.detect_object(image_obj, mark_objects)
            if display_feed:
                self.display_image(image_obj)
            rel_detection_finished_time = time.time() - detection_start_time

            if detected:
                rel_detected_duration += rel_detection_finished_time
            else:
                if rel_detected_duration > 0:
                    self._logger.debug("Lost the target. Last detected duration: [%s]", rel_detected_duration)
                rel_detected_duration = 0

            if rel_detected_duration > duration:
                ret_image = image_obj
                self._logger.debug("Image detected. Duration detected: [%s]", rel_detected_duration)
                break

            rel_elapsed_time += rel_detection_finished_time
        else:
            self._logger.debug("Unable to detect target for the given duration within the timeout. Elapsed time: [%s]", rel_elapsed_time)
        return ret_image

    """
    Public methods (Advance)
    """
    def display_video_feed(self):
        while True:
            image_obj, error = self.read_image()
            if not error:
                if self._enable_vfeed:
                    cv2.imshow(_SNAPSHOT_VFEED, image_obj)
                    if cv2.waitKey(1) & 0xFF == ord('q'):
                        break
                else:
                    cv2.destroyAllWindows()

    """
    Public methods (Manual)
    """
    def read_image(self):
        success, image_obj = self._video_capture_obj.read()
        return image_obj, (not success)

    def detect_object(self, image_obj, mark_objects=True):
        """
        Perform object detection on a given image
        :param image_obj: Image object (Image)
        :param mark_objects: Mark detected objects (bool)
        :return: Tuple(image_obj (Image), detected? (bool))
        """
        gray_image_obj = cv2.cvtColor(image_obj, cv2.COLOR_BGR2GRAY)
        detected_objs = self._object_classifier_obj.detectMultiScale(gray_image_obj,
                                                                     scaleFactor=1.1, minNeighbors=5, minSize=(30, 30),
                                                                     flags=cv2.CASCADE_SCALE_IMAGE)
        if mark_objects:
            for x, y, w, h in detected_objs:  # If target is detected
                cv2.rectangle(image_obj, (x, y), (x + w, y + h), (0, 255, 0), 2)
        return image_obj, (len(detected_objs) > 0)

    def display_image(self, image_obj):
        if self.enable_vfeed is True:
            cv2.imshow(_SNAPSHOT_VFEED, image_obj)
            cv2.waitKey(1)
        else:
            cv2.destroyAllWindows()

    """
    Public methods (Helpers)
    """
    @staticmethod
    def current_time():
        return datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")

    """
    Accessors
    """
    @property
    def enable_vfeed(self):
        return self._enable_vfeed

    """
    Mutators
    """
    @enable_vfeed.setter
    def enable_vfeed(self, state):
        self._enable_vfeed = state





if __name__ == "__main__":  # Test
    logging.basicConfig(format="%(message)s", level=logging.DEBUG)
    _app = Snapshot(camera_port=0)
    _image_obj = _app.search_for_object(3, 5, mark_objects=True)
    if _image_obj is not None:
        cv2.imshow(_SNAPSHOT_VFEED, _image_obj)
        while True:
            cv2.waitKey(1)
