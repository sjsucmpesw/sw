"""
Snapshot - An OpenCV application
"""

import argparse
import datetime
import logging
from Queue import Queue
import os
import sys
import time

logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)

import consolehandler
from logger import Logger
from snapshot import Snapshot
from threadmanager.threadmanager import ThreadManager
from transactor.client import TransactorClient

LOG = logging.getLogger(__name__)


def get_args():
    arg_parser = argparse.ArgumentParser()
    required_arguments = arg_parser.add_argument_group("Required Arguments")
    optional_arguments = arg_parser.add_argument_group("Optional Arguments")

    # REQUIRED ARGUMENTS

    # OPTIONAL ARGUMENTS
    optional_arguments.add_argument("--camera",
                                    type=int,
                                    metavar="<int>",
                                    default=0,
                                    help="Camera device ID")
    return arg_parser.parse_args()


def main(**kwargs):
    camera = kwargs.get("camera")

    logger_obj = Logger()
    logger_obj.add_stream_handler(stream=sys.stdout)
    logger_obj.add_file_handler(output_filepath=os.path.join(os.path.dirname(__file__), "{}.log".format(Snapshot.current_time())))
    global LOG
    LOG = logger_obj

    snapshot_obj = Snapshot(camera, logger=LOG)
    threadmanager_obj = ThreadManager(logger=LOG)
    transclient_obj = TransactorClient(logger=LOG)

    image_queue = Queue()

    threadmanager_obj.add_task(target=snapshot_obj.search_for_object,
                               args={"duration": 3, "timeout": 0, "display_feed": True},
                               periodic=0, ret_queue=image_queue)
    threadmanager_obj.add_task(target=transclient_obj.update_temp,
                               periodic=1)
    threadmanager_obj.add_task(target=_handle_image,
                               periodic=0,
                               args={"snapshot_obj": snapshot_obj,
                                     "image_queue": image_queue,
                                     "threadmanager_obj": threadmanager_obj,
                                     "transactorclient_obj": transclient_obj})
    threadmanager_obj.add_task(target=_handle_console,
                               periodic=0,
                               args=(threadmanager_obj, snapshot_obj, transclient_obj))

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        pass

    return 1


def _handle_image(threadmanager_obj, snapshot_obj, transactorclient_obj, image_queue):
    assert isinstance(threadmanager_obj, ThreadManager)
    assert isinstance(snapshot_obj, Snapshot)
    assert isinstance(transactorclient_obj, TransactorClient)
    assert isinstance(image_queue, Queue)

    image_obj = image_queue.get(block=True, timeout=None)
    if image_obj is not None:
        threadmanager_obj.add_task(target=snapshot_obj.save_image,
                                   auto_end=True, daemon=False,
                                   args={"image_obj": image_obj,
                                         "metadata": [current_time(),
                                                      "Temperature: {} F".format(transactorclient_obj.temperature)]})


def _handle_console(threadmanager_obj, snapshot_obj, transactorclient_obj):
    assert isinstance(threadmanager_obj, ThreadManager)
    assert isinstance(snapshot_obj, Snapshot)
    assert isinstance(transactorclient_obj, TransactorClient)

    command, console_input = consolehandler.handle_console()
    if command == consolehandler.ENABLE_FEED:
        snapshot_obj.enable_vfeed = True
    elif command == consolehandler.DISABLE_FEED:
        snapshot_obj.enable_vfeed = False
    elif command == consolehandler.QUIT:
        os._exit(0)  # Force this process and all child threads to close
    elif command == consolehandler.HELP:
        LOG.info("\n\nAvailable commands:\n\n{}\n".format("\n".join([
            consolehandler.HELP,
            consolehandler.ENABLE_FEED,
            consolehandler.DISABLE_FEED,
            consolehandler.QUIT
        ])))

    else:  # command == consolehandler.UNDEFINED
        LOG.warning("Undefined command: %s", console_input)


def current_time():
    return datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

if __name__ == "__main__":
    top_level_error = main(**vars(get_args()))
    sys.exit(top_level_error)
