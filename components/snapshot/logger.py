"""
Logger wrapper
"""

import logging

__author__ = "jtran"
__version__ = "0.1"

logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)

DEBUG = logging.DEBUG
INFO = logging.INFO
WARNING = logging.WARNING
ERROR = logging.ERROR

INFO_LOG_FORMATTER = logging.Formatter("%(levelname)s: %(message)s")
DEBUG_LOG_FORMATTER = logging.Formatter("[%(asctime)s] %(filename)s %(levelname)s: %(message)s")


class Logger(object):
    def __init__(self, logger_id=__name__):
        self._logger = logging.getLogger(logger_id)
        self._logger.propagate = False
        self._logger.setLevel(logging.DEBUG)
        self._stream_handlers = []
        self._file_handlers = []

    """
    Public methods
    """
    def add_stream_handler(self, stream, formatter=INFO_LOG_FORMATTER, level=INFO):
        sh = logging.StreamHandler(stream=stream)
        sh.setLevel(level)
        sh.setFormatter(formatter)
        self._logger.addHandler(sh)
        self._stream_handlers.append(sh)
        return sh

    def add_file_handler(self, output_filepath, mode="w", formatter=DEBUG_LOG_FORMATTER, level=DEBUG, encoding="cp437"):
        fh = logging.FileHandler(output_filepath, mode=mode, encoding=encoding)
        fh.setLevel(level)
        fh.setFormatter(formatter)
        self._logger.addHandler(fh)
        self._file_handlers.append(fh)
        return fh

    def remove_stream_handlers(self):
        for sh in self._stream_handlers:
            self._logger.removeHandler(sh)

    def remove_file_handlers(self):
        for fh in self._file_handlers:
            self._logger.removeHandler(fh)

    """
    Public methods (helpers)
    """
    @staticmethod
    def get_formatter(format_str):
        return logging.Formatter(format_str)

    """
    Overloaded methods
    """
    @property
    def debug(self):
        return self._logger.debug

    @property
    def info(self):
        return self._logger.info

    @property
    def warning(self):
        return self._logger.warning

    @property
    def error(self):
        return self._logger.error




if __name__ == "__main__":  # Test
    import sys
    log = Logger()
    #log.add_stream_handler(stream=sys.stdout)
    log.info("TEST")
