"""
Lightweight subclass Thread
"""

from Queue import Queue
from threading import Thread, Event
import time

__author__ = "jtran"
__version__ = "0.1"


class Task(Thread):
    def __init__(self, target, args=None,
                 daemon=False,
                 periodic=None,
                 ret_queue=None):
        """
        :param target: Task target function (function ptr)
        :param args: A iterable object of arguments (tuple or list or dict)
        :param daemon: Daemon flag (bool)
        :param periodic: Periodic execution timer (float)
        :param ret_queue: Queue for return values
        """
        Thread.__init__(self)
        assert target is not None
        self._target = target
        self._args = args
        self._is_done = Event()
        self._ret = None

        # Thread configuration
        self.setDaemon(daemon)

        # For periodic execution
        self._periodic = max(float(periodic), 0) if periodic is not None else None
        self._cancel = Event()

        if ret_queue is not None:
            assert isinstance(ret_queue, Queue)
        self._ret_queue = ret_queue

    """
    Public methods
    """
    def cancel_task(self):
        """
        Cancel this task
          - FOR PERIODIC TASK ONLY
        """
        self._cancel.set()

    """
    Overloaded methods
    """
    def run(self):
        if self._periodic is not None:
            while True:
                start_time = time.time()
                ret = self._execute_target(self._target, self._args)
                if self._ret_queue is not None:
                    self._ret_queue.put(item=ret, block=False)
                if self._cancel.is_set():
                    break
                while True:  # Wait until periodic timer expires
                    if (time.time() - start_time) > self._periodic:
                        break
        else:
            ret = self._execute_target(self._target, self._args)
            if self._ret_queue is not None:
                self._ret_queue.put(item=ret, block=False)

        self._ret = ret
        self._is_done.set()

    """
    Private method
    """
    @staticmethod
    def _execute_target(target, args):
        """
        Call the target function
        """
        if isinstance(args, dict):
            ret = target(**args)
        elif args is not None:
            ret = target(*args)
        else:
            ret = target()
        return ret

    """
    Accessors
    """
    @property
    def is_running(self):
        return self.is_alive

    @property
    def is_done(self):
        return self._is_done.is_set()

    @property
    def is_periodic(self):
        return self._periodic > 0.0

    @property
    def ret(self):
        return self._ret

    """
    Accessors (Advance)
    """
    def target(self):
        return self._target
