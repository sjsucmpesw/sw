"""
Thread Manager
  - Features:
    1. Thread pool
    2. Task creation
    3. Periodic task execution
    4. Automatic task termination on completion
"""

from collections import OrderedDict, namedtuple
import logging
from Queue import Queue
from threading import Thread

from prettytable import PrettyTable

from task import Task

__author__ = "jtran"
__version__ = "0.1"

TaskWrapper = namedtuple("TaskWrapper", ["task_obj", "auto_end"])


class ThreadManager(object):
    HEADER = "ThreadID Target Description"

    def __init__(self, name="", **kwargs):
        self._name = str(name)
        self._thread_pool = OrderedDict()  # A container of TaskWrapper

        self._thread_manager_thread = None

        self._logger = kwargs.get("logger", logging.getLogger(__name__))

    def __repr__(self):
        table = PrettyTable(self.HEADER.split(" "))
        for key, task_wrapper in self._thread_pool.items():
            description = ""
            if task_wrapper.task_obj.is_periodic:
                description = "Periodic: {}".format(task_wrapper.task_obj._periodic)
            table.add_row([key, str(task_wrapper.task_obj.target), description])

        return table.get_string()

    def __len__(self):
        return len(self._thread_pool)

    """
    Public methods
    """
    def start(self):
        """
        Start the task manager thread
        """
        self._thread_manager_thread = Task(target=self._monitor_task_pool, daemon=True)
        self._thread_manager_thread.start()

    def add_task(self, target, args=None, daemon=True, start=True, auto_end=False, periodic=None, ret_queue=None):
        """
        Create a new task
        :param target: Task target function (function ptr)
        :param args: A iterable object of arguments (tuple or list or dict)
        :param daemon: Daemon flag (bool)
        :param start: Start the task after creation
        :param auto_end: Mark the task to automatically terminate by the thread manager (bool)
        :param periodic: Periodic execution time; specify 0 to not make the task a periodic task (float)
        :param ret_queue: Queue for return values
        :return: Task ID (str)
        """
        new_task = Task(target=target, args=args, daemon=daemon, periodic=periodic, ret_queue=ret_queue)
        key = str(new_task.name)
        self._thread_pool[key] = TaskWrapper(new_task, auto_end)

        if start:
            new_task.start()

        return key

    """
    Public methods (Helper)
    """
    def is_done(self, task_id):
        """
        Check if a task is done
        :param task_id: Task ID (str)
        :return: Done (bool)
        """
        return self._thread_pool[task_id].task_obj.is_done

    def get_result(self, task_id, end=True):
        """
        Get the results of a task. Note: make sure the task is done to get a meaningful return value
        :param task_id: Task ID (str)
        :param end: Attempt to terminate the task after retrieving its return value (bool)
        :return: Task return value (<type>)
        """
        ret = self._thread_pool[task_id].task_obj.ret
        if end:
            self._end_task(task_id)
        return ret

    """
    Private methods
    """
    def _end_task(self, task_id):
        """
        End a task only if it is done
        :param task_id: The task ID (str)
        """
        if self.is_done(task_id):
            self._thread_pool[task_id].task_obj.join()
            self._thread_pool.pop(task_id)

    def _monitor_task_pool(self):
        """
        Continuously iterate through the thread pool dictionary and perform pending actions on each thread
        """
        while True:
            try:
                for task_id, task_wrapper in self._thread_pool.items():
                    # Task is flagged to automatically end and is done; end the task
                    if task_wrapper.auto_end and self.is_done(task_id):
                        self._end_task(task_id)
                    # Task is not running and is not done (meaning the task has not started); start the task
                    elif not task_wrapper.task_obj.is_running and not task_wrapper.task_obj.is_done:
                        task_wrapper.task_obj.start()
                    else:
                        pass
            except KeyError:
                pass

    """
    Accessors
    """




if __name__ == "__main__":  # Test

    import time
    def test_func(input, output):
        print input
        time.sleep(input)
        return output

    def test_func2(input, output):
        print input
        return output

    tm = ThreadManager()

    t1 = tm.add_task(target=test_func, args=[3, "test 1"], auto_end=True)
    t2 = tm.add_task(target=test_func, args=[5, "test 2"], auto_end=True)

    tm.add_task(target=test_func2, args={"input": 4, "output": 10}, periodic=1)
    tm.add_task(target=test_func2, args={"input": 3, "output": 10}, periodic=0.7)

    print tm

    tm.start()

    try:
        while True:
            pass
    except KeyboardInterrupt:
        pass
    finally:
        print tm

