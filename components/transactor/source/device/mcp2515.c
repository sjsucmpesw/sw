/*
 * mcp2515.c
 *
 *  Created on: Sep 4, 2016
 *      Author: YuYu
 */

#include "mcp2515.h"
#include "mcp2515_def.h"

#include <avr/io.h>

#include "exint.h"
#include "spi.h"
#include "gpio.h"

#define RST PINB0
#define CS 	PIND4
#define INT PIND3

Mcp2515_S mcp2515_vars;


bool mcp2515_init(char speed)
{
	mcp2515_vars.message_ready = false;

	init_master_spi0();

	// Initialize GPIO control signals and configure to idle levels
	set_dir_gpio(DATA_DIR_REG_D, CS, OUTPUT);    	// CS
	set_dir_gpio(DATA_DIR_REG_B, RST, OUTPUT) ;		// RST
	set_gpio(DATA_REG_D, CS);
	set_gpio(DATA_REG_B, RST);

	// Initialize external interrupt pin
	set_dir_gpio(DATA_DIR_REG_D, INT, INPUT);    	// INT
	enable_exint_1(FALLING_EDGE_SENSITIVE);

	// Reset the CAN controller
    clear_gpio(DATA_REG_B, RST); // Assert reset (active LOW)
    set_gpio(DATA_REG_B, RST); // Deassert reset

    clear_gpio(DATA_REG_D, CS);
    exchange_byte_spi0(SPI_RESET); // reset to go into configuration mode
    set_gpio(DATA_REG_D, CS);

#if DEBUG
    puts("In INIT");

    printf("CANSTAT    %x\n",rd_reg(CANSTAT)>>5);
    printf("SPEED     %x\n", speed);
#endif

    // continuous write will increment address pointer
    clear_gpio(DATA_REG_D, CS);
    exchange_byte_spi0(SPI_WRITE);
    exchange_byte_spi0(CNF3);

    // =================== FOR ALL SPEED =====================
    // setting bit timing to 16Tq
    // PS2 Length bits, (PHSEG2 + 1) x Tq , minimum valid setting is 2Tq
    // set to PS2 bit time length, and set PS1, (PHSEG1 + 1) x Tq , and PRSEG
    // setting all config to have 1Tq propagation segment length
    // sync bit by default is 1Tq
    // SJW set to 1Tq
    // Baud rate prescaler, Tq = 2(speed+1)/16MHz
    // Sample Point will be at 62.5% of bit timing

    exchange_byte_spi0(0x5 << PHSEG2); // setting PS2 to 6Tq
    exchange_byte_spi0(((1 << BTLMODE) | (0x7 << PHSEG1) | (0x0 << PRSEG))); // setting PS1 to 8Tq, 1Tq PRSEG
    exchange_byte_spi0(((0 << SJW) |(speed << BRP0))); // set Synchronization Jump Width to 1Tq, set baud rate prescaler

    exchange_byte_spi0((1 << RX1IE | 1 << RX0IE)); // CANINTE , Receive buffer 0/1 full interrupt enable
    set_gpio(DATA_REG_D, CS);

    if (rd_reg(CNF1) != speed)
        return false;

    wr_reg(BFPCTRL,0); // according to schematic, NC, so disable RXnBF (Hi-z)
    wr_reg(TXRTSCTRL,0); // according to schematic, NC, so disable TXnRTS (Hi-z)


#if FILTER
    wr_reg(RXB0CTRL, 0x0 << RXM0); // receive all valid messages standard or extended that meets filter
    wr_reg(RXB1CTRL, 0x0 << RXM1);// same

    // setting up each filter
    wr_Filt_reg(RXF0SIDH);
    wr_Filt_reg(RXF1SIDH);
    wr_Filt_reg(RXF2SIDH);
    wr_Filt_reg(RXF3SIDH);
    wr_Filt_reg(RXF4SIDH);
    wr_Filt_reg(RXF5SIDH);

    // setting up each masks
    wr_Mask_reg(RXM0SIDH);
#else
    wr_reg(RXB0CTRL, 0x3 << RXM0); // turn off mask/filter, receive any message
    wr_reg(RXB1CTRL, 0x3 << RXM1); // same

#endif

#if DEBUG
    printf("RXF0SIDH    %x\n", rd_reg(RXF0SIDH));
    printf("RXF0SIDL    %x\n", rd_reg(RXF0SIDL));
    printf("RXF0EID8    %x\n", rd_reg(RXF0EID8));
    printf("RXF0EID0    %x\n", rd_reg(RXF0EID0));

    printf("RXF1SIDH    %x\n", rd_reg(RXF1SIDH));
    printf("RXF1SIDL    %x\n", rd_reg(RXF1SIDL));
    printf("RXF1EID8    %x\n", rd_reg(RXF1EID8));
    printf("RXF1EID0    %x\n", rd_reg(RXF1EID0));

    printf("RXF2SIDH    %x\n", rd_reg(RXF2SIDH));
    printf("RXF2SIDL    %x\n", rd_reg(RXF2SIDL));
    printf("RXF2EID8    %x\n", rd_reg(RXF2EID8));
    printf("RXF2EID0    %x\n", rd_reg(RXF2EID0));

    printf("RXF3SIDH    %x\n", rd_reg(RXF3SIDH));
    printf("RXF3SIDL    %x\n", rd_reg(RXF3SIDL));
    printf("RXF3EID8    %x\n", rd_reg(RXF3EID8));
    printf("RXF3EID0    %x\n", rd_reg(RXF3EID0));

    printf("RXF4SIDH    %x\n", rd_reg(RXF4SIDH));
    printf("RXF4SIDL    %x\n", rd_reg(RXF4SIDL));
    printf("RXF4EID8    %x\n", rd_reg(RXF4EID8));
    printf("RXF4EID0    %x\n", rd_reg(RXF4EID0));

    printf("RXF5SIDH    %x\n", rd_reg(RXF5SIDH));
    printf("RXF5SIDL    %x\n", rd_reg(RXF5SIDL));
    printf("RXF5EID8    %x\n", rd_reg(RXF5EID8));
    printf("RXF5EID0    %x\n", rd_reg(RXF5EID0));
#endif

#if NORMAL_OPERATION
    wr_reg(CANCTRL, 0 << REQOP); // set to normal operation
#elif LOOPBACK_OPERATION
    wr_reg(CANCTRL, 2 << REQOP); // set to loopback operation
#else
    wr_reg(CANCTRL, 3 << REQOP); // set to listen-only operation
#endif

#if DEBUG
    printf("CNF3    %x\n", rd_reg(CNF3));
    printf("CNF2    %x\n", rd_reg(CNF2));
    printf("CNF1    %x\n", rd_reg(CNF1));


    printf("CANCTRL    %x\n", rd_reg(CANCTRL)>>5);
    printf("CANINTE    %x\n", rd_reg(CANINTE));
#endif

    return true;
}


void wr_reg(char addr, char data)
{
	clear_gpio(DATA_REG_D, CS);
    exchange_byte_spi0(SPI_WRITE);
    exchange_byte_spi0(addr);
    exchange_byte_spi0(data);
    set_gpio(DATA_REG_D, CS);

}

char rd_reg(char addr)
{

    char data;

    clear_gpio(DATA_REG_D, CS);
    exchange_byte_spi0(SPI_READ);
    exchange_byte_spi0(addr);
    data = exchange_byte_spi0(0x0);
    set_gpio(DATA_REG_D, CS);

    return data;
}

void wr_Filt_reg(char addr)
{
    uint8_t upper = PID_REPLY >> 3;
    uint8_t lower = (PID_REPLY << 5 | 0 << 3) & ~(3 << 0);

    clear_gpio(DATA_REG_D, CS);
    exchange_byte_spi0(SPI_WRITE);
    exchange_byte_spi0(addr);   // starting from 0x0, 0x4, 0x8, 0x10, 0x14, 0x18
    exchange_byte_spi0(upper); //put ID filter into SID[10:3] of RXFnSIDH
    exchange_byte_spi0(lower); //put ID filter into SID[2:0], which is in bit[7:5] of RXFnSIDL
    exchange_byte_spi0(0); //RXFnEID8 -- 0x2, 0x6, 0xA, 0x12, 0x16, 0x1A, set to 0 b/c not used
    exchange_byte_spi0(0); //RXFnEID0 -- 0x3, 0x7, 0xB, 0x13, 0x17, 0x1B, set to 0 b/c not used
    set_gpio(DATA_REG_D, CS);
}

void wr_Mask_reg(char addr)
{
	clear_gpio(DATA_REG_D, CS);
    exchange_byte_spi0(SPI_WRITE);

    exchange_byte_spi0(addr); // starting from 0x20
    exchange_byte_spi0(MASK >> 3); // RXM0SIDH
    exchange_byte_spi0((MASK << 5) & ~(3 << 0)); // RXM0SIDL
    exchange_byte_spi0(0); // RXM0EID8
    exchange_byte_spi0(0); // RXM0EID0, 0x23

    exchange_byte_spi0(MASK >> 3); // RXM1SIDH , 0x24
    exchange_byte_spi0((MASK << 5) & ~(3 << 0)); // RXM1SIDL
    exchange_byte_spi0(0); // RXM1EID8
    exchange_byte_spi0(0); // RXM1EID0, 0x27
    set_gpio(DATA_REG_D, CS);
}

void bit_modify(char addr, char mask, char data)
{
	clear_gpio(DATA_REG_D, CS);
    exchange_byte_spi0(SPI_BIT_MODIFY);
    exchange_byte_spi0(addr);
    exchange_byte_spi0(mask);
    data = exchange_byte_spi0(data);
    set_gpio(DATA_REG_D, CS);

}

char rd_status(char opcode) {
    char data;

    clear_gpio(DATA_REG_D, CS);

    exchange_byte_spi0(opcode);
    data = exchange_byte_spi0(0x0);

    set_gpio(DATA_REG_D, CS);

    return data;
}

bool check_free_buffer(){
    char status;
    status = rd_status(SPI_READ_STATUS); // check TX buffer (TXBnCNTRL.TXREQ),
    if(status == 0x54)                          // 0 1 0 1   0 1 0 0
        return false;                           //   ^   ^     ^
                                                //  TX2  TX1   TX0
    return true;

}

bool get_message(canMessage *message) {
    char status;
    char addr;
    uint16_t SID;
    int dataLength;

    status = rd_status(SPI_RX_STATUS); // check which buffer received message is in
#if DEBUG
    puts("In get_message");
    printf("Status:    %x\n", status);
    printf("CANSTAT:    %x\n", rd_reg(CANSTAT));
    printf("CANINTF:   %x\n", rd_reg(CANINTF));
    printf("EFLG:   %x\n", rd_reg(EFLG));

    printf("RXB0CTRL:    %x\n", rd_reg(RXB0CTRL));
    printf("RXB1CTRL:    %x\n", rd_reg(RXB1CTRL));
#endif

    if (status & (1 << 6)) { // Message in RXB0
        addr = SPI_READ_RX;
#if DEBUG
        puts("Message in RXB0");
#endif
    }
    else if (status & (1 << 7)) { // Message in RXB1
        addr = (SPI_READ_RX | 0x4);
#if DEBUG
        puts("Message in RXB1");
#endif
    }
    else {
        // no message
#if DEBUG
        puts("No message");
#endif
        return false;
    }

    // starting address should be either 0x61 or 0x71

    clear_gpio(DATA_REG_D, CS);
    exchange_byte_spi0(addr);

    // getting Standard Identifier bits [10:0]        bits 15   --------------------->    0
    SID =  exchange_byte_spi0(0) << 3;                   // 0 0 0 0 0 1 1 1 1 1 1 1 1 0 0 0
    SID |= exchange_byte_spi0(0) >> 5;                   // 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1
                                                         // 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 1

    message->id = SID;

    // address pointer auto increments to next register after each read
    exchange_byte_spi0(0);  // skipping EIDH reg
    exchange_byte_spi0(0);  // skipping EIDL reg

    // reading DLC (data length code) register, only need last 4 bits
    dataLength = (exchange_byte_spi0(0) & 0xf);
    message->length = dataLength;

    // check if it is a standard data frame or remote frame
    message->rtr = (status & (1 << 3)) ? 1: 0;

    // store x amount of data bytes according to DLC
    for(int i =0; i < dataLength; i++) {
        message->data[i] = exchange_byte_spi0(0);
    }
    // end transaction
    set_gpio(DATA_REG_D, CS);

    // clear receive interrupt flag to reset interrupt condition (need to be cleared to get new message)
    if (status & (1 << 6)) // check which receive buffer has a message
    {
        bit_modify(CANINTF, 1 << RX0IF, 0); // clear receive buffer 0 flag bit
    }
    else
    {
        bit_modify(CANINTF, 1 << RX1IF, 0); // clear receive buffer 1 flag bit
    }
#if DEBUG
    read_msg();
#endif
    return true;
}

bool send_message(canMessage *message) {
    char status;
    char addr;
    char dataLength;

    mcp2515_vars.message_ready = false;

#if DEBUG
    puts("In send_message");
    printf("CANSTAT:    %x\n", rd_reg(CANSTAT));
    printf("CANINTF:   %x\n", rd_reg(CANINTF));
    printf("EFLG:   %x\n", rd_reg(EFLG));

    printf("TXB0CTRL:    %x\n", rd_reg(TXB0CTRL));
    printf("TXB1CTRL:    %x\n", rd_reg(TXB1CTRL));
    printf("TXB2CTRL:    %x\n", rd_reg(TXB2CTRL));
#endif

    status =rd_status(SPI_READ_STATUS);

    if (!(status & (1 << 2))) // TXB0CNTRL.TXREQ, check TX0 buffer if it is empty
    {
        addr =  0x0; // addr = 0x40, address points to TXB0SIDH (0x31)
    }
    else if (!(status & (1 << 4))) // check TX1 buffer if it is empty
    {
        addr =  0x2; // addr = 0x42, address points to TXB1SIDH (0x41)
    }
    else if (!(status & (1 << 6))) // check TX2 buffer if it is empty
    {
        addr =  0x4; // addr = 0x44, address points to TXB2SIDH (0x51)
    }
    else
    {
        // all buffer full/busy
        return false;
    }

    // start transaction , loading data into registers before transmission
    clear_gpio(DATA_REG_D, CS);
    exchange_byte_spi0((SPI_WRITE_TX | addr)); // address depends on top

    exchange_byte_spi0(message->id >> 3);  // SID bits [10:3]
    exchange_byte_spi0((message->id & 0x7) << 5);  // SID bits [2:0] into bits[7:5] of TXBnSIDL

    exchange_byte_spi0(0); // skip EID regs
    exchange_byte_spi0(0);

    dataLength = (message->length & 0xf); // DLC to be sent

    if(message->rtr)
    	exchange_byte_spi0((1 << RTR | dataLength)); // RTR frame has length, but no header
    else {
    	exchange_byte_spi0(dataLength);

        for(int i=0; i< dataLength; i++) {
        	exchange_byte_spi0(message->data[i]); // send x length amount of data
        }
    }
    // end loading register
    set_gpio(DATA_REG_D, CS);


    // start transmission of data from one of the 3 TX buffers
    clear_gpio(DATA_REG_D, CS);
    addr = (addr == 0) ? 1: addr; // TX2 = bit 2 , TX1 = bit 1 , TX0 = bit 0
    exchange_byte_spi0((SPI_RTS | addr));  // request to send, initiate message transmission
    set_gpio(DATA_REG_D, CS);
#if DEBUG
    printf("TXB0CTRL:    %x\n", rd_reg(TXB0CTRL));
    printf("TXB1CTRL:    %x\n", rd_reg(TXB1CTRL));
    printf("TXB2CTRL:    %x\n", rd_reg(TXB2CTRL));

    read_msg();
#endif
    return true;
}


ISR(INT1_vect)
{
	mcp2515_vars.message_ready = true;
}
