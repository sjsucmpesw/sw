#include "clock.h"
#include "bmp180.h"
#include "bmp180_data.h"

#include <util/delay.h>

#include "i2c.h"

// BMP180 I2C Interface
#define BMP180_DEFAULT_SLA_ADDR 0xEE
#define I2C_MBUFF_SIZE 16U

// BMP180 Register Locations
#define OUT_LSB_ADDR 0xF7
#define OUT_MSB_ADDR 0xF6
#define CTRL_MEAS_ADDR 0xF4

// BMP180 control measurements register values
#define CTRL_MEAS_TEMP 0x2E
#define CTRL_MEAS_PRESSURE_1X_SAMPLE 0x34
#define CTRL_MEAS_PRESSURE_2X_SAMPLE 0x74
#define CTRL_MEAS_PRESSURE_4X_SAMPLE 0xB4
#define CTRL_MEAS_PRESSURE_8X_SAMPLE 0xF4

// Temperature
#define MAX_TEMP_CONVERSION_TIME_MS 4.5F
#define TEMP_SCALE 0.1F

#define POW2_15 32768U
#define POW2_11 2048U
#define POW2_4 16U

static float compute_temp(uint16_t uncompensated_temp);
static inline float celsius_to_fahrenheit(float celsius);


void BMP180_init(void)
{
	I2C_master_init();
}


float BMP180_read_temp(void)
{
	uint8_t i2c_mbuff[I2C_MBUFF_SIZE] = {0};

    I2CMHandle_S i2c_temp_sensor_handle = {
        .sla_addr = BMP180_DEFAULT_SLA_ADDR,
        .reg_addr = 0x00,
        .data_buff = i2c_mbuff,
        .data_buff_size = sizeof(i2c_mbuff),
        .count = 1U,
    };

    // Start temperature conversion
    i2c_temp_sensor_handle.reg_addr = CTRL_MEAS_ADDR;
    i2c_temp_sensor_handle.data_buff[0] = CTRL_MEAS_TEMP;
    i2c_temp_sensor_handle.count = 1U;
    uint8_t wr_count = I2C_master_write(&i2c_temp_sensor_handle);
    while (!(I2C_is_master_trans_done()));

    _delay_ms(MAX_TEMP_CONVERSION_TIME_MS);

    // Read uncompensated temperature
    i2c_temp_sensor_handle.reg_addr = OUT_MSB_ADDR;
    i2c_temp_sensor_handle.count = 2U;
    uint8_t rd_count = I2C_master_read(&i2c_temp_sensor_handle);
    while (!(I2C_is_master_trans_done()));

    uint16_t uncompensated_temp = ((i2c_temp_sensor_handle.data_buff[0] << 8U) | i2c_temp_sensor_handle.data_buff[1]);  // ADC_H | ADC_L

    return celsius_to_fahrenheit(compute_temp(uncompensated_temp));
}


static float compute_temp(uint16_t uncompensated_temp)
{
	float x1 = ((float)uncompensated_temp - (float)AC6) * ((float)AC5 / (float)POW2_15);
    float x2 = ((float)MC * (float)POW2_11) / (x1 + (float)MD);
    float temp = (((x1 + x2) + 8.0F) / (float)POW2_4) * TEMP_SCALE;
    return temp; 
}


static inline float celsius_to_fahrenheit(float celsius)
{
	return ((celsius * 1.8F) + 32.0F);
}
