#include "clock.h"
#include "transactor.h"
#include "transactorInterface.h"

#include <util/delay.h>

#include "bmp180.h"
#include "usart.h"

#define BMP180_TEMP_READ_PERIOD 100.0F
#define USART0_BAUD 9600U

static void read_temp_then_send(void);


void transactor_init(void)
{
    BMP180_init();
    init_uart_usart0(USART0_BAUD);
}


void transactor_app_task(void)
{
    while (1 == 1) {  // Task loop
    	while (1 == 1) {
    		while (!(is_rx_ready_usart0()));
    		SerialFrame_U command = {0};
    		command.all = rx_usart0();
    		switch (command.fields.cmd) {
    			case CMD_TEMP:
    				read_temp_then_send();
    				break;
    			default:
    				break;
    		}
    	}
    }  // Task loop
}


static void read_temp_then_send(void)
{
	float temp = BMP180_read_temp();
	tx_usart0((uint8_t)temp);
	while (!(is_tx_empty_usart0()));
}