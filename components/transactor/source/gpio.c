#include <avr/io.h>
#include "gpio.h"

/* Physical signal levels */
#define HIGH 1
#define LOW 0



void set_dir_gpio(Sfr* data_dir_reg, uint8_t pin_offset, uint8_t mode)
{
	switch (mode) {
	case OUTPUT:
		*data_dir_reg |= (1U << pin_offset);
		break;
	default: // INPUT
		*data_dir_reg &= ~(1U << pin_offset);
		break;
	}
}


void set_gpio(Sfr* data_reg, uint8_t pin_offset)
{
	*data_reg |= (1U << pin_offset);
}


void clear_gpio(Sfr* data_reg, uint8_t pin_offset)
{
	*data_reg &= ~(1U << pin_offset);
}


void toggle_gpio(Sfr* data_reg, uint8_t pin_offset)
{
	*data_reg ^= (1U << pin_offset);
}


uint8_t get_gpio(Sfr* input_reg, uint8_t pin_offset)
{
	return (*input_reg & (1U << pin_offset));
}
