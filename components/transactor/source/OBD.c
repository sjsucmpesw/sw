/*
 * OBD.c
 *
 *  Created on: Sep 4, 2016
 *      Author: YuYu
 */

#include "OBD.h"
#include "mcp2515.h"

#include <avr/interrupt.h>

typedef enum {
	SM_IDLE 			=	0U,
	SM_REQUESTTOSENDCAN,
	SM_REQUESTWAIT,
	SM_GETMESSAGE
} ObdState_E;

typedef struct {
	ObdState_E sm_state;
} ObdSmVars_S;

static ObdSmVars_S obdvars;
ObdInput_S obd_input;
ObdOutput_S obd_output;

static canMessage message;

static void init_sm_obd(void);
static void init_message(canMessage* message, char pid);


bool obd_init(uint8_t can_baud)
{
	bool success = mcp2515_init((char)can_baud);
	init_sm_obd();
	return success;
}


void sm_obd(void)
{
	switch (obdvars.sm_state) {
	case SM_IDLE:
		/* Reset output and flags */
		obd_output.done = false;
		obd_output.error = ERROR_NONE;

		if((obd_input.go) && (obd_input.pid != NOOP)) {
			obdvars.sm_state = SM_REQUESTTOSENDCAN;
		}
		else {
			// Do nothing
		}
		break;

	case SM_REQUESTTOSENDCAN:
		// Assemble a CAN frame and command the CAN controller to perform a CAN transaction
		init_message(&message, obd_input.pid);
		if (send_message(&message)) {
			obdvars.sm_state = SM_REQUESTWAIT;
		}
		else {
			obd_output.error = ERROR_FAIL_TO_SEND_MSG;
			obdvars.sm_state = SM_IDLE;
		}
		break;

	case SM_REQUESTWAIT:
		// Check if MCP2515 message is ready
		if (mcp2515_vars.message_ready) {
			obdvars.sm_state = SM_GETMESSAGE;
		}
		else {
			// Do nothing
		}
		break;

	case SM_GETMESSAGE:			// Get CAN message via CAN controller SPI interface
		obd_output.done = true;

		if (get_message(&message)) {
			if (message.data[2] == VEHICLE_SPEED) {
				obd_output.vehicle_speed = message.data[3] / KMTOMILE;
			}
			else {
				// Do nothing
			}
		}
		else {
			obd_output.error = ERROR_FAIL_TO_GET_MSG;
		}
		obdvars.sm_state = SM_IDLE;
		break;
	}
}


static void init_sm_obd(void)
{
	obd_output.done = false;
	obd_output.error = ERROR_NONE;
	obd_output.vehicle_speed = 0U;
	obd_input.go = false;
	obd_input.pid = NOOP;
	obdvars.sm_state = SM_IDLE;
}


static void init_message(canMessage* message, char pid)
{
	/* Build message */
	message->id = PID_REQUEST;
	message->rtr = 0; // Data Frame
	message->length = 8; // # of bytes to be transferred
	message->data[0] = 0x02;
	message->data[1] = 0x01;
	message->data[2] = pid;
	message->data[3] = 0x00;
	message->data[4] = 0x00;
	message->data[5] = 0x00;
	message->data[6] = 0x00;
	message->data[7] = 0x00;
}
