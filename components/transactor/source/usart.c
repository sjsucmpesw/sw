#include "clock.h"
#include "usart.h"

#include <avr/interrupt.h>
#include <avr/io.h>

#define BAUD_PRESCALE(baud, pclk) ((pclk / (16UL * baud)) - 1)


void init_uart_usart0(uint32_t baud_rate)
{
	/* Reset USART registers */
	// Control and status; this action should also reset the TX/RX buffers
	UCSR0A = 0x0;
	UCSR0B = 0x0;
	UCSR0C = 0x0;

	// USART Baud Rate
	UBRR0L = 0x0;
	UBRR0H = 0x0;

	/* Configure USART to perform in Asynchronous mode */
	UCSR0C &= ~(0x3 << UMSEL00);  // UMSEL0[1:0]

	/* Configure 8-bit data frame */
	UCSR0C |= (0x3 << UCSZ00);   // UCSZ0[1:0]
	UCSR0B &= ~(0x1 << UCSZ02);  // UCSZ0[2]

	/* Disable parity bit */
	UCSR0C &= ~(0x3 << UPM00);  // UPM0[1:0]

	/* Configure 1 stop bit */
	UCSR0C &= ~(0x1 << USBS0);

	/* Configure baud rate */
	uint16_t baud_reg_value = BAUD_PRESCALE(baud_rate, F_CPU);

	UBRR0L = baud_reg_value;				   // UBRR0[7:0]
	UBRR0H = (0x0F & (baud_reg_value >> 8U));  // UBRR0[11:8]; UBRR0H[3:0]

	/* Configure IOs for USART0 operation */
	// Enable RX pull-up
	DDRD &= ~(0x1 << PD0);
	PORTD |= (0x1 << PD0);
	UCSR0B |= (0x1 << TXEN0);
	UCSR0B |= (0x1 << RXEN0);
}


void enable_rx_interrupt_usart0(void)
{
	UCSR0B |= (1 << RXCIE0);
	sei();
}


void enable_tx_interrupt_usart0(void)
{
	UCSR0B |= (1 << TXCIE0);
	sei();
}


void tx_usart0(uint8_t tx_data)
{
	UDR0 = tx_data;
}


uint8_t rx_usart0(void)
{
	uint8_t rx_data = UDR0;
	return rx_data;
}


bool is_tx_empty_usart0(void)
{
	return (UCSR0A & (1 << UDRE0));
}


bool is_rx_ready_usart0(void)
{
	return (UCSR0A & (1 << RXC0));
}


bool is_frame_error_usart0(void)
{
	return (UCSR0A & (1 << FE0));
}
