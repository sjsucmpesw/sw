#include "ctrlDefs.h"

//#include "FreeRTOS.h"
//#include "task.h"

#include "transactor.h"

static void init(void);
static void rtos_init(void);


int main(void) 
{
    (void)init();
    (void)transactor_app_task();
    (void)rtos_init();
    //(void)vTaskStartScheduler();  // Returns only if insufficient RAM
    return 1;               		// System should never return
}


static void init(void)
{
    transactor_init();
}


static void rtos_init(void)
{
    // Define tasks here
}

