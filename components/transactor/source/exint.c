#include "exint.h"

void enable_exint_1(uint8_t trigger_type)
{
	/* Reset configuration for EXINT1 */
	EICRA &= (0x3 << ISC10); // ISC1[1:0]

	/* Configure EXINT trigger type */
	EICRA |= (trigger_type << ISC10); // ISC1[1:0]

	/* Enable EXINT1 */
	EIMSK |= (1U << INT1);

	/* Enable global interrupts */
	sei();
}


void enable_pci(uint8_t group, uint8_t index)
{
	PCICR |= (1U << group);
	switch (group) {
	case PCI_GROUP0:
		PCMSK0 |= (1U << index);
		break;
	case PCI_GROUP1:
		PCMSK1 |= (1U << index);
		break;
	case PCI_GROUP2:
		PCMSK2 |= (1U << index);
		break;
	default:
		break;
	}
}


bool get_pci_flags(uint8_t group)
{
	return (PCIFR & (1U << group));
}
