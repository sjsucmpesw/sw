#include <avr/io.h>

#include "tc.h"


void init_timer_tc1(void)
{
	/* Using TC1 16-bit timer/counter */

	/* Timer/Counter Mode: Normal; compare-on-Match disabled */
	TCCR1A = 0x00;
	TCCR1B = 0x00;

	/* Timer period = 1 / ((PCLK / N) / 2^16) where N is the prescale */
	/* Configure prescale to 1 */
	TCCR1B |= (0x1 << CS10);

	/* Reset the counter */
	TCNT1L = 0x0000;
}


void init_timer_tc2(void)
{
	/* Using TC2 8-bit timer/counter */

	/* Timer/Counter Mode: Normal; compare-on-Match disabled */
	TCCR2A = 0x00;
	TCCR2B = 0x00;

	/* Timer period = 1 / ((PCLK / N) / 2^8) where N is the prescale */
	/* Configure prescale to 64 */
	TCCR2B |= (0x4 << CS20);

	/* Reset the counter */
	TCNT2 = 0x00;
}


void init_pwm_tc0(bool en_pwm_a, bool en_pwm_b)
{
	/* Using TC0 8-bit timer/counter */

	/* Timer/Counter Mode: Fast PWM (TOP: 0xFF) */
	TCCR0A = 0x00;
	TCCR0B = 0x00;
	TCCR0A |= (0x1 << WGM01) | (0x1 << WGM00);

	/* PWM freq = ((PCLK / N) / 2^8) where N is the prescale */
	/* Configure prescale to 64 */
	TCCR0B |= (0x3 << CS00);


	/* Enable output of PWM pins */
	if (en_pwm_a) {
		/* Compare-on-Match: OC0A (clear-on-match; set-on-BOTTOM) (non-inverting) */
		TCCR0A |= (0x2 << COM0A0);
		DDRD |= (0x1 << PIND6);	// PD6
	}
	if (en_pwm_b) {
		/* Compare-on-Match: OC0B (clear-on-match; set-on-BOTTOM) (non-inverting) */
		TCCR0A |= (0x2 << COM0B0);
		DDRD |= (0x1 << PIND5);	// PD5
	}
}


void init_pwm_tc2(bool en_pwm_a, bool en_pwm_b)
{
	/* Using TC2 8-bit timer/counter */

	/* Timer/Counter Mode: Fast PWM (TOP: 0xFF) */
	TCCR2A = 0x00;
	TCCR2B = 0x00;
	TCCR2A |= (0x1 << WGM21) | (0x1 << WGM20);

	/* PWM freq = ((PCLK / N) / 2^8) where N is the prescale */
	/* Configure prescale to 1 */
	TCCR2B |= (0x1 << CS20); // CS2[2:0]

	/* Enable output of PWM pins */
	if (en_pwm_a) {
		/* Compare-on-Match: OC2A (clear-on-match; set-on-BOTTOM) */
		TCCR2A |= (0x2 << COM2A0);
		DDRB |= (0x1 << PINB3);	// PB3
	}
	if (en_pwm_b) {
		/* Compare-on-Match: OC2B (clear-on-match; set-on-BOTTOM) */
		TCCR2A |= (0x2 << COM2B0);
		DDRD |= (0x1 << PIND3);	// PD3
	}
}


void init_squarewave_tc0(bool en_squarewave_a, bool en_squarewave_b)
{
	/* Using TC0 8-bit timer/counter */

	/* Reset control registers */
	TCCR0A = 0x00;
	TCCR0B = 0x00;

	/* Timer/Counter Mode: CTC */
	TCCR0A |= (0x1 << WGM01);
	OCR0A = 0x3F; // TOP

	/* Square wave freq = ((PCLK / N) / 2^8) where N is the prescale */
	/* Configure prescale to 256 */
	TCCR0B |= (0x4 << CS00); // CS0[2:0]

	/* Reset the counter */
	TCNT0 = 0x00;

	if (en_squarewave_a) {
		/* Compare-on-Match: OC0A (Toggle on match) */
		TCCR0A |= (0x1 << COM0A0); // COM0A[1:0]
		DDRD |= (0x1 << PIND6); // PD6
	}
	else {
		TCCR0A &= ~(0x3 << COM0A0); // COM0A[1:0]
		DDRD &= ~(0x1 << PIND6); // PD6
	}
	if (en_squarewave_b) {
		/* Compare-on-Match: OC0B (Toggle on match) */
		TCCR0A |= (0x1 << COM0B0); // COM0B[1:0]
		DDRD |= (0x1 << PIND5); // PD5
	}
	else {
		TCCR0A &= ~(0x3 << COM0B0); // COM0B[1:0]
		DDRD &= ~(0x1 << PIND5); // PD5
	}
}


void init_squarewave_tc1(bool en_squarewave_a, bool en_squarewave_b)
{
	/* Using TC1 16-bit timer/counter */

	/* Reset control registers */
	TCCR1A = 0x00;
	TCCR1B = 0x00;

	/* Timer/Counter Mode: CTC */
	TCCR1B |= (0x1 << WGM12);
	OCR1A = 0x3FFF;

	/* Square wave freq = ((PCLK / N) / 2^16) where N is the prescale */
	/* Configure prescale to 64 */
	TCCR1B |= (0x3 << CS10); // CS1[2:0]

	/* Reset the counter */
	TCNT1 = 0x0000;

	if (en_squarewave_a) {
		/* Compare-on-Match: OC1A (Toggle on match) */
		TCCR1A |= (0x1 << COM1A0); // COM1A[1:0]
		DDRB |= (0x1 << PINB1); // PB1
	}
	else {
		TCCR1A &= ~(0x3 << COM1A0); // COM1A[1:0]
		DDRB &= ~(0x1 << PINB1); // PB1
	}
	if (en_squarewave_b) {
		/* Compare-on-Match: OC1B (Toggle on match) */
		TCCR1A |= (0x1 << COM1B0); // COM1B[1:0]
		DDRB |= (0x1 << PINB2); // PB2
	}
	else {
		TCCR1A &= ~(0x3 << COM1B0); // COM1B[1:0]
		DDRB &= ~(0x1 << PINB2); // PB2
	}
}


void init_squarewave_tc2(bool en_squarewave_a, bool en_squarewave_b)
{
	/* Using TC2 8-bit timer/counter */

	/* Timer/Counter Mode: Normal */
	TCCR2A = 0x00;
	TCCR2B = 0x00;

	/* Square wave freq = ((PCLK / N) / 2^8) where N is the prescale */
	/* Configure prescale to 256 */
	TCCR2B |= (0x6 << CS20); // CS2[2:0]

	/* Reset the counter */
	TCNT2 = 0x00;

	if (en_squarewave_a) {
		/* Compare-on-Match: OC2A (Set on match) */
		TCCR2A |= (0x3 << COM2A0); // COM2A[1:0]
		DDRB |= (0x1 << PINB3); // PB3
	}
	else {
		TCCR2A &= ~(0x3 << COM2A0); // COM2A[1:0]
		DDRB &= ~(0x1 << PINB3); // PB3
	}
	if (en_squarewave_b) {
		/* Compare-on-Match: OC2B (Set on match) */
		TCCR2A |= (0x3 << COM2B0); // COM2B[1:0]
		DDRD |= (0x1 << PIND3); // PD3
	}
	else {
		TCCR2A &= ~(0x3 << COM2B0); // COM2B[1:0]
		DDRD &= ~(0x1 << PIND3); // PD3
	}
}


void wait_tc1(uint32_t ticks)
{
	/* Using TC1 16-bit timer */
	/* Timer period = 1 / ((PCLK / N) / 2^16) where N is the prescale */

	TCNT1 = 0x00;	// Reset the counter
	uint32_t wait = 1 * ticks;
	uint32_t counter = 0;
	while (!(counter >= wait)) {
		if (TIFR1 & (1 << TOV1)) {
			TIFR1 |= (1 << TOV1);	// Reset the overflow flag
			counter++;
		}
	}
}


void wait_tc2(uint32_t ticks)
{
	/* Using TC2 8-bit timer */
	/* Timer period = 1 / ((PCLK / N) / 2^8) where N is the prescale */

	TCNT2 = 0x00;	// Reset the counter
	uint32_t wait = 1 * ticks;
	uint32_t counter = 0;
	while (!(counter >= wait)) {
		if (TIFR2 & (1 << TOV2)) {
			TIFR2 |= (1 << TOV2);	// Reset the overflow flag
			counter++;
		}
	}
}


void set_pwm_duty_cycle_tc0_a(uint8_t duty_cycle_8U)
{
	if (duty_cycle_8U > 0) {
		DDRD |= (1 << 6U);
	}
	else {
		DDRD &= ~(1 << 6U);
	}
	OCR0A = duty_cycle_8U; // PD6
}


void set_pwm_duty_cycle_tc0_b(uint8_t duty_cycle_8U)
{
	if (duty_cycle_8U > 0) {
		DDRD |= (1 << 5U);
	}
	else {
		DDRD &= ~(1 << 5U);
	}
	OCR0B = duty_cycle_8U; // PD5
}


void set_pwm_duty_cycle_tc2_a(uint8_t duty_cycle_8U)
{
	if (duty_cycle_8U > 0) {
		DDRB |= (1 << 3U);
	}
	else {
		DDRB &= ~(1 << 3U);
	}
	OCR2A = duty_cycle_8U; // PB3
}


void set_pwm_duty_cycle_tc2_b(uint8_t duty_cycle_8U)
{
	if (duty_cycle_8U > 0) {
		DDRD |= (1 << 3U);
	}
	else {
		DDRD &= ~(1 << 3U);
	}
	OCR2B = duty_cycle_8U; // PD3
}
