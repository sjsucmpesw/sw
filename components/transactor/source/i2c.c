#include "i2c.h"
#include "i2cStates.h"

#include <avr/interrupt.h>
#include <avr/io.h>

#include "gpio.h"
#include "usart.h"

#define ACK() (TWCR |= (1 << TWEA))
#define NACK() (TWCR &= ~(1 << TWEA))
#define START() (TWCR |= (1 << TWSTA))
#define CLEAR_START() (TWCR &= ~(1 << TWSTA))
#define STOP() (TWCR |= (1 << TWSTO))
#define CLEAR_INT() (TWCR |= (1 << TWINT))
#define STATUS() (TWSR & (0xF8))  // TWSR[7:3]
#define ADDR_WR(addr) (addr & ~(1 << 0))
#define ADDR_RD(addr) (addr | (1 << 0))

I2CMHandle_S *g_curr_mhandle;

typedef enum {
    WR_MODE=0,
    RD_MODE,
} I2CMode_E;

typedef struct {
    uint8_t count;
    I2CMode_E mode;
} MTransaction_S;
MTransaction_S g_mtransaction;

static inline void stop(void);


void I2C_master_init(void)
{
    sei();  // Enable global interrupts

    /*
     * SCL_freq = CPU_FREQ / (16 + 2(TWBR) * PrescalerValue)
     */
    TWSR = 0x0;
    TWBR = 24;

    TWCR = 0x0;
    TWCR |= (1 << TWIE);  // Enable I2C interrupt
    TWCR |= (1 << TWEN);  // Enable I2C

    set_dir_gpio(DATA_DIR_REG_C, PINC4, OUTPUT);
    set_dir_gpio(DATA_DIR_REG_C, PINC5, OUTPUT);

    // DEBUG
    //init_uart_usart0(9600U);
}   


uint8_t I2C_master_read(I2CMHandle_S *mhandle)
{
    g_curr_mhandle = mhandle;
    g_curr_mhandle->done = false;
    g_mtransaction.count = 0U;
    g_mtransaction.mode = RD_MODE;
    START();

    return g_mtransaction.count;
}


uint8_t I2C_master_write(I2CMHandle_S *mhandle)
{
    g_curr_mhandle = mhandle;
    g_curr_mhandle->done = false;
    g_mtransaction.count = 0U;
    g_mtransaction.mode = WR_MODE;
    START();

    return g_mtransaction.count;
}


bool I2C_is_master_trans_done(void)
{
    return g_curr_mhandle->done;
}


static inline void stop(void)
{
    g_curr_mhandle->done = true;
    STOP();
}


ISR(TWI_vect)
{
    uint8_t status = STATUS();

    // DEBUG
    // tx_usart0(status);
    // while (!(is_tx_empty_usart0()));

    switch (status) {
        case START_SENT:
            TWDR = ADDR_WR(g_curr_mhandle->sla_addr);
            CLEAR_START();
            break;

        case REAP_START_SENT:
            TWDR = ADDR_RD(g_curr_mhandle->sla_addr);
            CLEAR_START();
            break;

        case SLA_W_SENT_ACK_REC:
            TWDR = g_curr_mhandle->reg_addr;  // Transmit register address to read or write
            break;

        case SLA_W_SENT_NACK_REC:
            stop();
            break;

        case DAT_SENT_ACK_REC:
            if (g_mtransaction.mode == RD_MODE) {
                TWDR = ADDR_RD(g_curr_mhandle->sla_addr);
                START();  // Repeated START
            }
            else {
                if (g_mtransaction.count < g_curr_mhandle->count) {
                    TWDR = g_curr_mhandle->data_buff[g_mtransaction.count++];
                }
                else {
                    stop();
                }    
            }
            break;
        
        case DAT_SENT_NACK_REC:
            stop();
            break;

        case SLA_R_SENT_ACK_REC:
            if (g_curr_mhandle->count == 1) {
                NACK();
            }
            else {
                ACK();
            }
            break;

        case SLA_R_SENT_NACK_REC:
            stop();
            break;

        case DAT_REC_ACK_SENT:
            g_curr_mhandle->data_buff[g_mtransaction.count++] = TWDR;
            if (g_mtransaction.count + 1 >= g_curr_mhandle->count) {
                NACK();
            }
            else {
                ACK();
            }
            break;

        case DATA_REC_NACK_SENT:
            g_curr_mhandle->data_buff[g_mtransaction.count++] = TWDR;
            stop();
            break;

        default:
            stop();
            break;
    }

    CLEAR_INT();  // Clear interrupt flag
}