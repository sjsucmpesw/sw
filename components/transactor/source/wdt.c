#include "wdt.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/wdt.h>


void init_wdt(void)
{
	wdt_reset();

	/* Reset configuration */
	WDTCSR = 0x0;

	// Beware of WD Change Enable behavior and compiler optimization

	// Using inline ASM to configure WDT;
	wdt_enable(WDTO_500MS);
	wdt_reset();
}


void reset_wdt(void)
{
	wdt_reset();
}


void sys_rst_wdt(void)
{
	/* Reset configuration */
	WDTCSR = 0x0;

	wdt_enable(WDTO_15MS);

	while(1 == 1);	// Wait until reset occurs
}


ISR(WDT_vect)
{
	// Do nothing
}
