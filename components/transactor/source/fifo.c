#include "fifo.h"

#include <stdlib.h>

#define ROLLOVER_FLAG(ptr, size) (ptr & size)
#define ADDR_MASK(ptr, size) (ptr & (size - 1U))

static uint8_t power(uint8_t base, uint8_t exponent);


void fifo_init(Fifo_S* fifo, uint8_t addr_size_bits)
{
	uint8_t size = power(2U, addr_size_bits);

	fifo->size = size;
	fifo->read_ptr = 0U;
	fifo->write_ptr = 0U;
	fifo->buffer = (uint8_t*) malloc(size);
}


void fifo_write(Fifo_S* fifo, uint8_t data)
{
	fifo->buffer[ADDR_MASK(fifo->write_ptr, fifo->size)] = data;
    
    /* Discard least recent data if writing to a full FIFO */ 
    if (is_fifo_full(fifo)) {
        fifo->read_ptr++;
    } 
    // No else case
    
    fifo->write_ptr++;
}


uint8_t fifo_read(Fifo_S* fifo)
{
	uint8_t data = fifo->buffer[ADDR_MASK(fifo->read_ptr, fifo->size)];
    
    /* Prevent read pointer from incrementing when attempting to read from an empty FIFO */
    if (!is_fifo_empty(fifo)) {
        fifo->read_ptr++;
    }
	// No else case
    
	return data;
}


void fifo_reset(Fifo_S* fifo)
{
	fifo->read_ptr = 0U;
	fifo->write_ptr = 0U;
}


bool is_fifo_empty(Fifo_S* fifo)
{
	bool empty = ((ADDR_MASK(fifo->read_ptr, fifo->size) == ADDR_MASK(fifo->write_ptr, fifo->size)) &&
			      (ROLLOVER_FLAG(fifo->read_ptr, fifo->size) == ROLLOVER_FLAG(fifo->write_ptr, fifo->size)));
	return empty;
}


bool is_fifo_full(Fifo_S* fifo)
{
	bool full = ((ADDR_MASK(fifo->read_ptr, fifo->size) == ADDR_MASK(fifo->write_ptr, fifo->size)) &&
			     (ROLLOVER_FLAG(fifo->read_ptr, fifo->size) != ROLLOVER_FLAG(fifo->write_ptr, fifo->size)));
	return full;
}


static uint8_t power(uint8_t base, uint8_t exponent)
{
	uint8_t result = 1U;
	for (uint8_t count=0U; count<exponent; count++) {
		result *= base;
	}
	return result;
}
