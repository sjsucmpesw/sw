#include "spi.h"

#include <avr/interrupt.h>
#include <avr/io.h>

#define MOSI_PIN PINB3
#define SCK_PIN PINB5
#define SS_PIN PINB2


void init_master_spi0(void)
{
	/* Reset control, status and data registers */
	SPCR = 0x0;
	SPSR = 0x0;
	SPDR = 0x0;

	/* Configure MSb transmit first */
	SPCR &= ~(0x1 << DORD);

	/* Configure clock polarity - idle LOW */
	SPCR &= ~(0x1 << CPOL);

	/* Configure clock phase: sample first edge */
	SPCR &= ~(0x1 << CPHA);

	/* Configure SCK clock rate: pclk / 16 */
	SPCR |= (0x1 << SPR0);  	// SPR[1:0]
	SPSR &= ~(0x1 << SPI2X);  // SPI2X0[0]

	/* Configure SCK, MOSI, and SS as an output */
	DDRB |= (0x1 << SCK_PIN);
	DDRB |= (0x1 << MOSI_PIN);
	DDRB |= (0x1 << SS_PIN);
	PORTB |= (0x1 << SS_PIN); // Asserting SS pin HIGH; if SS pin is driven LOW, the SPI controller will transition to slave mode

	/* Configure master mode */
	SPCR |= (0x1 << MSTR);

	/* Enable SPI */
	SPCR |= (0x1 << SPE);
}


void enable_interrupt_spi0(void)
{
	SPCR |= (0x1 << SPIE);
	sei();
}


char exchange_byte_spi0(char tx_data)
{
	SPDR = tx_data;
	while (!(SPSR & (0x1 << SPIF))) {
		// Wait for the completion of the transaction
	}
	char rx_data = SPDR;
	return rx_data;
}


void send_byte_spi0(char tx_data)
{
	SPDR = tx_data;
}


char read_byte_spi0(void)
{
	char rx_data = SPDR;
	return rx_data;
}


bool is_transfer_done_spi0(void)
{
	return (SPSR & (0x1 << SPIF));
}


bool is_write_collision_spi0(void)
{
	return (SPSR & (0x1 << WCOL));
}
