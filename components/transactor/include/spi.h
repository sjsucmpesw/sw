#ifndef SPI_H
#define SPI_H

#include <stdbool.h>


void init_master_spi0(void);
void enable_interrupt_spi0(void);

char exchange_byte_spi0(char tx_data);
void send_byte_spi0(char tx_data);
char read_byte_spi0(void);

bool is_transfer_done_spi0(void);
bool is_write_collision_spi0(void);


#endif  // SPI_H