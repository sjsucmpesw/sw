#ifndef EXINT_H
#define EXINT_H

/*
 * Two (2) types of interrupts
 * External Interrupt (EXINT)
 * 	- Configurable trigger
 *
 * Pin Change Interrupt (PCI)
 * 	- Level sensitive trigger per pin (index)
 */
 
#include <stdbool.h>
#include <stdint.h>

#include <avr/interrupt.h>
#include <avr/io.h>

/*
 * External Interrupt groups and mapped pins
 * EXINT0: PIND2
 *  - Use function header: ISR(INT0_vect)
 *
 * EXINT1: PIND3
 *  - Use function header: ISR(INT1_vect)
 *
 *  Note: require "#include <avr/interrupt.h>" for ISR function header
 */

/* External Interrupt trigger type */
#define LOW_LEVEL_SENSITIVE 		(0x0)
#define LEVEL_SENSITIVE 			(0x1)
#define FALLING_EDGE_SENSITIVE		(0x2)
#define RISING_EDGE_SENSITIVE		(0x3)

/* Pin Change Interrupt groups */
#define PCI_GROUP0 (PCIE0 & PCIF0)	// PCINT[7:0]
#define PCI_GROUP1 (PCIE1 & PCIF1)	// PCINT[14:8]
#define PCI_GROUP2 (PCIE2 & PCIF2)	// PCINT[23:16]

void enable_exint_1(uint8_t trigger_type);

void enable_pci(uint8_t group, uint8_t index);
bool is_pci_triggered(uint8_t group);


#endif  // EXINT_H