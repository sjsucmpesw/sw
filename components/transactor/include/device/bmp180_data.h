/*
 * BMP180 Calibration Data 
 */

#ifndef BMP180_DATA_H
#define BMP180_DATA_H

#define AC5 24819U
#define AC6 20057U
#define MC -11786
#define MD 2930

#endif  // BMP180_DATA_H