/*
 * mcp2515.h
 *
 *  Created on: Sep 4, 2016
 *      Author: YuYu
 */

#ifndef MCP2515_H_
#define MCP2515_H_

#include <stdbool.h>
#include <stdint.h>


#define FILTER          1
#define DEBUG           0

#define PID_REPLY       0x7E8
#define MASK            0x7FF

#define NORMAL_OPERATION 	1
#define LOOPBACK_OPERATION 	0

typedef struct {
	uint16_t id;
    int8_t rtr :1;
    char length :4;
    char data[8];
} canMessage;

typedef struct {
	bool message_ready;
} Mcp2515_S;

extern Mcp2515_S mcp2515_vars;

/*
 * CAN controller interface
 *
 */

bool mcp2515_init(char speed); // actual init of mcp2515

void wr_reg(char addr, char data);
char rd_reg(char addr);
void bit_modify(char addr, char mask, char data);
char rd_status(char opcode);
bool check_free_buffer(void); // check all 3 TX buffers if they are full
bool get_message(canMessage* message);
bool send_message(canMessage* message);

void wr_Filt_reg(char addr);
void wr_Mask_reg(char addr);


#endif /* MCP2515_H_ */

