#ifndef BMP180_H
#define BMP180_H


void BMP180_init(void);
float BMP180_read_temp(void);


#endif  // BMP180_H