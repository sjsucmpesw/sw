#ifndef TRANSACTOR_H
#define TRANSACTOR_H


void transactor_init(void);
void transactor_app_task(void);


#endif  // TRANSACTOR_H