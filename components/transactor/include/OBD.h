/*
 * OBD.h
 *
 *  Created on: Sep 4, 2016
 *      Author: YuYu
 */

#ifndef CAN_H_
#define CAN_H_

#include <stdbool.h>
#include <stdint.h>


/* CAN baud rate */
#define CAN_BAUD_33     14      // CAN speed at 33 kbps
#define CAN_BAUD_50     9       // CAN speed at 50 kbps
#define CAN_BAUD_83     5       // CAN speed at 83 kbps
#define CAN_BAUD_100    4       // CAN speed at 100 kbps
#define CAN_BAUD_125    3       // CAN speed at 125 kbps
#define CAN_BAUD_166    2       // CAN speed at 166 kbps
#define CAN_BAUD_250    1       // CAN speed at 250 kbps
#define CAN_BAUD_500    0       // CAN speed at 500 kbps

/* PARAMETER ID (PID) */
#define NOOP				0x00
#define ENGINE_COOLANT_TEMP 0x05
#define ENGINE_RPM          0x0C
#define VEHICLE_SPEED       0x0D
#define MAF_SENSOR          0x10
#define O2_VOLTAGE          0x14
#define THROTTLE            0x11

#define PID_REQUEST         0x7DF

#define KMTOMILE            1.60

typedef enum {
	ERROR_NONE 				= 	0,
	ERROR_FAIL_TO_SEND_MSG 	= 	1,
	ERROR_FAIL_TO_GET_MSG	=	2,
} ErrorCode_E;

typedef struct {
	bool go;
	uint8_t pid;
} ObdInput_S;

typedef struct {
	bool done;
	ErrorCode_E error;
	uint8_t vehicle_speed;
} ObdOutput_S;

extern ObdInput_S obd_input;
extern ObdOutput_S obd_output;

bool obd_init(uint8_t can_baud);
void sm_obd(void);


#endif /* CAN_H_ */

