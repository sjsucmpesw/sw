#ifndef FIFO_H
#define FIFO_H

#include <stdbool.h>
#include <stdint.h>


typedef struct {
	uint8_t* buffer;
	uint8_t write_ptr;
	uint8_t read_ptr;
	uint8_t size;
} Fifo_S;


void fifo_init(Fifo_S* fifo, uint8_t addr_size_bits);

void fifo_write(Fifo_S* fifo, uint8_t data);
uint8_t fifo_read(Fifo_S* fifo);

void fifo_reset(Fifo_S* fifo);

bool is_fifo_empty(Fifo_S* fifo);
bool is_fifo_full(Fifo_S* fifo);


#endif  // FIFO_H