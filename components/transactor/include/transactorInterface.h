#ifndef TRANSACTORINTERFACE_H
#define TRANSACTORINTERFACE_H

#include <stdint.h>


typedef	union {
	uint8_t all;
	struct {
		uint8_t cmd : 4;
		uint8_t argument : 4;
	} fields;
} SerialFrame_U;


typedef enum {
	CMD_NOOP = 0x0,
	CMD_TEMP,
	CMD_LAST = 0x10		// CMD limit
} Command_E;


typedef enum {
	RESPONSE_ACK = 0x0,
	RESPONSE_NACK,
} Response_E;




#endif  // TRANSACTORINTERFACE_H
