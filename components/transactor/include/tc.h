#include <stdbool.h>
#include <stdint.h>


void init_timer_tc1(void);
void init_timer_tc2(void);
void init_pwm_tc0(bool en_pwm_a, bool en_pwm_b);
void init_pwm_tc2(bool en_pwm_a, bool en_pwm_b);
void init_squarewave_tc0(bool en_squarewave_a, bool en_squarewave_b);
void init_squarewave_tc1(bool en_squarewave_a, bool en_squarewave_b);
void init_squarewave_tc2(bool en_squarewave_a, bool en_squarewave_b);
void wait_tc1(uint32_t ticks);
void wait_tc2(uint32_t ticks);
void set_pwm_duty_cycle_tc0_a(uint8_t duty_cycle_8U);
void set_pwm_duty_cycle_tc0_b(uint8_t duty_cycle_8U);
void set_pwm_duty_cycle_tc2_a(uint8_t duty_cycle_8U);
void set_pwm_duty_cycle_tc2_b(uint8_t duty_cycle_8U);
