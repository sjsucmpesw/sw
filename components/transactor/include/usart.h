#ifndef USART_H
#define USART_H

#include <stdint.h>
#include <stdbool.h>


void init_uart_usart0(uint32_t baud_rate);
void enable_rx_interrupt_usart0(void);
void enable_tx_interrupt_usart0(void);

void tx_usart0(uint8_t tx_data);
uint8_t rx_usart0(void);

bool is_tx_empty_usart0(void);
bool is_rx_ready_usart0(void);
bool is_frame_error_usart0(void);


#endif  // USART_H