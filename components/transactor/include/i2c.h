#ifndef I2C_H
#define I2C_H

#include <stdbool.h>
#include <stdint.h>


typedef struct {
    uint8_t sla_addr;
    uint8_t reg_addr;
    uint8_t *data_buff;
    uint8_t data_buff_size;
    uint8_t count;
    bool done;
} I2CMHandle_S;


void I2C_master_init(void);
uint8_t I2C_master_read(I2CMHandle_S *mhandle);
uint8_t I2C_master_write(I2CMHandle_S *mhandle);
bool I2C_is_master_trans_done(void);

#endif  // I2C_H