#ifndef GPIO_H
#define GPIO_H

#include <stdint.h>

#include <avr/io.h>

typedef volatile uint8_t Sfr;

/* GPIO direction modes */
#define OUTPUT 1
#define INPUT 0

/* GPIO special function registers */
#define DATA_DIR_REG_B 	(&DDRB)
#define DATA_DIR_REG_C 	(&DDRC)
#define DATA_DIR_REG_D 	(&DDRD)
#define DATA_REG_B 		(&PORTB)
#define DATA_REG_C 		(&PORTC)
#define DATA_REG_D 		(&PORTD)
#define INPUT_REG_B		(&PINB)
#define INPUT_REG_C		(&PINC)
#define INPUT_REG_D		(&PIND)


/*
 * For pin offsets: use the macro PINXn
 * where 'X' is the port and 'n' is the pin offset
 *
 * Example: PINB5; PINC0; PIND2;
 */


void set_dir_gpio(Sfr* data_dir_reg, uint8_t pin_offset, uint8_t mode);
void set_gpio(Sfr* data_reg, uint8_t pin_offset);
void clear_gpio(Sfr* data_reg, uint8_t pin_offset);
void toggle_gpio(Sfr* data_reg, uint8_t pin_offset);
uint8_t get_gpio(Sfr* input_reg, uint8_t pin_offset);


#endif  // GPIO_H