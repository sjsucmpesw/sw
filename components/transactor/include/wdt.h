#ifndef WDT_H
#define WDT_H


void init_wdt(void);
void reset_wdt(void);
void sys_rst_wdt(void);


#endif  // WDT_H