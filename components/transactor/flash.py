"""
Atmega 328P Firmware Downloader - Using USBTINY
"""

import argparse
import logging
import os
import subprocess
import sys

logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)

__author__ = "jtran"
__version__ = "1.0"

PART = "m328p"
PROGRAMMER = "usbtiny"

FAILURE = r"""
_____ _    ___ _    _   _ ____  _____
|  ___/ \  |_ _| |  | | | |  _ \| ____|
| |_ / _ \  | || |  | | | | |_) |  _|
|  _/ ___ \ | || |__| |_| |  _ <| |___
|_|/_/   \_\___|_____\___/|_| \_\_____|
"""

SUCCESS = r"""
 ____  _   _  ____ ____ _____ ____ ____
/ ___|| | | |/ ___/ ___| ____/ ___/ ___|
\___ \| | | | |  | |   |  _| \___ \___ \
 ___) | |_| | |__| |___| |___ ___) |__) |
|____/ \___/ \____\____|_____|____/____/
"""


def get_args():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("--hex",
                            type=str,
                            metavar="<file path>",
                            required=True,
                            help="Hex filepath")
    return arg_parser.parse_args()


def main(**kwargs):
    hex_filepath = kwargs.get("hex")
    if not os.path.isfile(hex_filepath):
        logging.error("Unable to find: %s", hex_filepath)
        return 1  # Return early

    # Flash firmware onto target; using HEX file in current directory
    flash_target_cmd = ["avrdude",
                        "-p", PART,
                        "-c", PROGRAMMER,
                        "-v", "-U",
                        "flash:w:{}:i".format(hex_filepath)]
    logging.debug("Using command: %s", flash_target_cmd)
    error = subprocess.call(flash_target_cmd)
    if error:
        logging.error("Failed to flash target!")

    return error


if __name__ == "__main__":
    top_level_error = main(**vars(get_args()))

    if not top_level_error:
        logging.info(SUCCESS)
    else:
        logging.info(FAILURE)

    sys.exit(top_level_error)
